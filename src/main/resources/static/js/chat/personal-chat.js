// jQuery(document) - $(document)
// $(document).ready(function() {});

$(document).ready(() => {

    // object - '', [], {}, true, 3

    // button.btn   - tag button with class 'btn'
    // #message     - id (search by id)
    // .btn         - search by class

    // $('#send-button').on('click', function() {});
    $('#send-button').on('click', sendMessage);

});

function sendMessage() {
    // var
    // let
    // const
    const text = $('#message').val(); // get value

    // ==
    // !=
    // ===
    // !==

    // undefined
    // null
    if (text) {
        sendMessageToServer(text);
    }
}

function renderMessage(message) {
    const tag = `<div class="wc-message">${message}</div>`; // '<div class="wc-message">Text</div>'
    $('#message-box').append($(tag)).animate({
        scrollTop: $(this).height()
    }, 'slow');

    $('#message').val(''); // set value
}

function sendMessageToServer(text) {
    const receiverId = $('#room-header').attr('user-id');

    const messageData = {
        text: text
    };

    $.ajax({
        url: '/chats/personal/' + receiverId + '/message',
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(messageData)
    }).done((resp) => {
        renderMessage(resp.text);
    }).fail((resp) => {
        console.log('fail');
    });


}