// jQuery(document) - $(document)
// $(document).ready(function() {});

$(document).ready(() => {

    // object - '', [], {}, true, 3

    // button.btn   - tag button with class 'btn'
    // #message     - id (search by id)
    // .btn         - search by class

    // $('#send-button').on('click', function() {});
    $('#send-button').on('click', sendMessage);

});

function sendMessage() {
    // var
    // let
    // const
    const text = $('#message').val(); // get value

    // ==
    // !=
    // ===
    // !==

    // undefined
    // null
    if (text) {
        sendMessageToServer(text);
    }
}

function renderMessage(message) {
    const tag = `<div class="wc-message">${message}</div>`; // '<div class="wc-message">Text</div>'
    $('#message-box').append($(tag)).animate({
        scrollTop: $(this).height()
    }, 'slow');

    $('#message').val(''); // set value
}

function sendMessageToServer(text) {
    // room id
    const messageData = {
        text: text,
        roomId: $('#room-header').attr('room-id') // get attribute value
        // set attribute value: $('#room-header').attr('room-id', 'value')
    };

    // AJAX
    $.ajax({
        url: '/rooms/' + messageData.roomId + '/message',  // /rooms/1/message
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(messageData)
        // deprecated
        // success: function(response) {
        //
        // },
        // error: function(response) {
        //
        // }
    }).done((resp) => {
        // { id: 1, text: 'asdfasdfasdfafs'}
        renderMessage(resp.text);
    }).fail((resp) => {
        console.log('fail');
    });

    // when function will be done, then function2 executes
    // $.when(function1]() {}).done(function2() {})

}