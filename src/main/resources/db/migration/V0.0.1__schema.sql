create table if not exists users(
  id serial primary key,
  login varchar(30) not null unique,
  password varchar(100) not null
);

insert into users (login, password)
values
       ('user', 'user_password'),
       ('admin', 'admin_password');
