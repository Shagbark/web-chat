create table if not exists user_details (
  id            integer primary key,
  first_name    varchar(100) not null,
  last_name     varchar(100) not null,
  avatar_path   text, -- varchar(255)
  age           integer,
  constraint user_details_id_fkey foreign key (id) references users(id)
);

insert into user_details
values
    (1, 'Ivan', 'Ivanov', '/avatars/default.png', 23),
    (2, 'Petr', 'Petrov', '/avatars/default.png', 64);

create table if not exists personal_messages (
  id    serial primary key,
  sender_id integer not null,
  receiver_id integer not null,
  text      text not null,
  send_date timestamp not null,
  constraint personal_messages_sender_id foreign key (sender_id) references users(id),
  constraint personal_messages_receiver_id foreign key (receiver_id) references users(id)
);