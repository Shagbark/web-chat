package org.levelup.chat.repository;

import org.levelup.chat.domain.entity.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<UserEntity, Integer> {

    // find - select
    // by login
    UserEntity findByLogin(String login);

}
