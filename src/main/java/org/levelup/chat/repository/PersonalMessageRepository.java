package org.levelup.chat.repository;

import org.levelup.chat.domain.entity.PersonalMessageEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonalMessageRepository extends CrudRepository<PersonalMessageEntity, Integer> {

    Iterable<PersonalMessageEntity> findLast10BySenderIdAndReceiverId(Integer senderId, Integer receiverId);

    @Query(
            value = "select * from personal_messages " +
                    "where (sender_id = :senderId and receiver_id = :receiverId) or " +
                    "(sender_id = :receiverId and receiver_id = :senderId)",
            nativeQuery = true
    )
    Iterable<PersonalMessageEntity> findLast10Message(@Param("senderId") Integer senderId,
                                                      @Param("receiverId") Integer receiverId);

}
