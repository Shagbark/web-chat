package org.levelup.chat.repository;

import org.levelup.chat.domain.entity.MessageEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MessageRepository extends CrudRepository<MessageEntity, Integer> {

    Iterable<MessageEntity> findByRoomId(Integer roomId);

}
