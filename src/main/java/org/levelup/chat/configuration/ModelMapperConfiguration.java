package org.levelup.chat.configuration;

import org.levelup.chat.domain.dto.Message;
import org.levelup.chat.domain.dto.UserAvatar;
import org.levelup.chat.domain.entity.PersonalMessageEntity;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.spi.MappingContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

@Configuration
public class ModelMapperConfiguration {

    @Bean
    public ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();

        configurePersonalMessageToMessages(modelMapper);

        return modelMapper;
    }

    private void configurePersonalMessageToMessages(ModelMapper modelMapper) {
        modelMapper.addMappings(new PropertyMap<PersonalMessageEntity, Message>() {
            @Override
            protected void configure() {}
        }).setPostConverter(context -> {
            PersonalMessageEntity source = context.getSource();
            Message destination = context.getDestination();

            destination.setUser(new UserAvatar("/avatars/default.png",
                    source.getSender().getLogin()));
            return destination;
        });

    }

}
