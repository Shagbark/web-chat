package org.levelup.chat.controller;

import org.levelup.chat.domain.dto.Message;
import org.levelup.chat.domain.dto.MessageData;
import org.levelup.chat.service.MessageService;
import org.levelup.chat.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/rooms")
public class RoomController {

    private final RoomService roomService;
    private final MessageService messageService;

    @Autowired
    public RoomController(RoomService roomService, MessageService messageService) {
        this.roomService = roomService;
        this.messageService = messageService;
    }

    @GetMapping
    public String displayRooms(Model model) {
        model.addAttribute("rooms", roomService.findAll());
        return "rooms";
    }

    // /rooms?parameter=423&parameter2=423
    // @GetMapping("/{roomId}/load/{usersId}") // path parameter
    @GetMapping("/{roomId}") // path parameter
    public String displayRoom(@PathVariable("roomId") final Integer roomId,
                              // @PathVariable("usersId") final Integer usersId,
                              Model model) {
        model.addAttribute("room", roomService.findById(roomId));
        // HashMap<String, Object>
        // put("messages", Collection);
        model.addAttribute("messages", messageService.findAllMessagesInRoom(roomId));
        return "room-by-id";
    }


    @ResponseBody
    @PostMapping("/{roomId}/message")
    public Message saveMessage(@PathVariable final Integer roomId,
                               @RequestBody MessageData messageData,
                               @CookieValue("WC_SESSION") final String sid) {
        return messageService.saveMessage(sid, messageData);
    }


}
