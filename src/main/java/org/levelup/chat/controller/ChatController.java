package org.levelup.chat.controller;

import org.levelup.chat.domain.dto.Message;
import org.levelup.chat.service.AuthorizationSessionService;
import org.levelup.chat.service.MessageService;
import org.levelup.chat.service.RoomService;
import org.levelup.chat.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@Controller
@RequestMapping("/chats")
public class ChatController {

    private final UserService userService;
    private final RoomService roomService;
    private final MessageService messageService;

    private final AuthorizationSessionService authSessionService;

    @Autowired
    public ChatController(UserService userService, RoomService roomService, MessageService messageService, AuthorizationSessionService authSessionService) {
        this.userService = userService;
        this.roomService = roomService;
        this.messageService = messageService;
        this.authSessionService = authSessionService;
    }

    @GetMapping
    public String displayAvailableChats(Model model){
        model.addAttribute("rooms", roomService.findAll());
        model.addAttribute("users", userService.findAll());
        return "chats";
    }

    // Я - Петров  - /personal/2
    // Петров - Я - /personal/8
    @GetMapping("/personal/{receiverId}")
    public String displayPersonalChat(@PathVariable final Integer receiverId,
                                      @CookieValue("WC_SESSION") final String sid,
                                      Model model) {
        // get last 10 messages
        Integer currentUserId = authSessionService.findUserIdBySessionId(sid);
        Collection<Message> messages = messageService
                .findLast10PersonalMessages(currentUserId, receiverId);

        model.addAttribute("messages", messages);
        model.addAttribute("receiverId", receiverId);
        return "personal-messages";
    }

    @ResponseBody
    @PostMapping("/personal/{receiverId}/message")
    public Message savePersonalMessage(@PathVariable final Integer receiverId,
                                       @RequestBody Message message,
                                       @CookieValue("WC_SESSION") final String sid) {
        return messageService.savePersonalMessage(sid, receiverId, message.getText());
    }

}
