package org.levelup.chat.service.impl;

import org.levelup.chat.domain.dto.Room;
import org.levelup.chat.domain.entity.RoomEntity;
import org.levelup.chat.exception.ChatException;
import org.levelup.chat.repository.RoomRepository;
import org.levelup.chat.service.RoomService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class RoomServiceImpl extends AbstractService implements RoomService {

    private final RoomRepository roomRepository;

    @Autowired
    protected RoomServiceImpl(ModelMapper modelMapper, RoomRepository roomRepository) {
        super(modelMapper);
        this.roomRepository = roomRepository;
    }

    @Override
    public Collection<Room> findAll() {
        Iterable<RoomEntity> values = roomRepository.findAll();
        return findAllEntities(values, Room.class);
    }

    @Override
    public Room findById(Integer roomId) {
        return roomRepository.findById(roomId) // Optional<RoomEntity>
                .map(entity -> modelMapper.map(entity, Room.class))
                .orElseThrow(ChatException::new);
    }

}
