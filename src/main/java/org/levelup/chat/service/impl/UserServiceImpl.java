package org.levelup.chat.service.impl;

import org.levelup.chat.domain.dto.User;
import org.levelup.chat.domain.entity.UserEntity;
import org.levelup.chat.repository.UserRepository;
import org.levelup.chat.service.AuthenticationService;
import org.levelup.chat.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class UserServiceImpl extends AbstractService implements UserService {

    private final AuthenticationService authenticationService;
    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(AuthenticationService authenticationService, UserRepository userRepository, ModelMapper modelMapper) {
        super(modelMapper);
        this.authenticationService = authenticationService;
        this.userRepository = userRepository;
    }

    @Override
    public boolean auth(String login, String password) {
        return authenticationService.authenticate(login, password);
    }

    @Override
    public Collection<User> findAll() {
        Iterable<UserEntity> iterable = userRepository.findAll();
        return findAllEntities(iterable, User.class);
    }

}
