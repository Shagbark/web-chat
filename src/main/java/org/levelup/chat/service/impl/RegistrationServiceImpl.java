package org.levelup.chat.service.impl;

import org.levelup.chat.domain.dto.RegistrationRequest;
import org.levelup.chat.domain.entity.UserDetailsEntity;
import org.levelup.chat.domain.entity.UserEntity;
import org.levelup.chat.repository.UserDetailsRepository;
import org.levelup.chat.repository.UserRepository;
import org.levelup.chat.service.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RegistrationServiceImpl implements RegistrationService {

    private final UserRepository userRepository;
    private final UserDetailsRepository userDetailsRepository;

    @Autowired
    public RegistrationServiceImpl(UserRepository userRepository, UserDetailsRepository userDetailsRepository) {
        this.userRepository = userRepository;
        this.userDetailsRepository = userDetailsRepository;
    }

    @Override
    @Transactional
    public void register(RegistrationRequest request) {
        UserEntity user = new UserEntity();
        user.setLogin(request.getLogin());
        user.setPassword(request.getPassword());

        UserEntity entity = userRepository.save(user);

        UserDetailsEntity details = new UserDetailsEntity();
        details.setFirstName(request.getFirstName());
        details.setLastName(request.getLastName());
        details.setAge(request.getAge());
        details.setId(entity.getId());
        details.setUser(entity);

        userDetailsRepository.save(details);
    }

}
