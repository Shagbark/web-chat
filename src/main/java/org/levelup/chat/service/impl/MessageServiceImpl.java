package org.levelup.chat.service.impl;

import org.levelup.chat.domain.dto.Message;
import org.levelup.chat.domain.dto.MessageData;
import org.levelup.chat.domain.dto.UserAvatar;
import org.levelup.chat.domain.entity.MessageEntity;
import org.levelup.chat.domain.entity.PersonalMessageEntity;
import org.levelup.chat.domain.entity.RoomEntity;
import org.levelup.chat.domain.entity.UserEntity;
import org.levelup.chat.repository.MessageRepository;
import org.levelup.chat.repository.PersonalMessageRepository;
import org.levelup.chat.repository.RoomRepository;
import org.levelup.chat.repository.UserRepository;
import org.levelup.chat.service.AuthorizationSessionService;
import org.levelup.chat.service.MessageService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@Service
public class MessageServiceImpl extends AbstractService implements MessageService {

    private final AuthorizationSessionService authSessionService;
    private final UserRepository userRepository;
    private final RoomRepository roomRepository;
    private final MessageRepository messageRepository;
    private final PersonalMessageRepository personalMessageRepository;

    @Autowired
    protected MessageServiceImpl(ModelMapper modelMapper, AuthorizationSessionService authSessionService,
                                 UserRepository userRepository, RoomRepository roomRepository, MessageRepository messageRepository, PersonalMessageRepository personalMessageRepository) {
        super(modelMapper);
        this.authSessionService = authSessionService;
        this.userRepository = userRepository;
        this.roomRepository = roomRepository;
        this.messageRepository = messageRepository;
        this.personalMessageRepository = personalMessageRepository;
    }

    @Override
    public Collection<Message> findAllMessagesInRoom(Integer roomId) {
        Iterable<MessageEntity> values = messageRepository.findByRoomId(roomId);
        Collection<Message> allEntities = findAllEntities(values, Message.class);
        return allEntities.stream()
                .peek(entity -> entity.setUser(new UserAvatar("/avatars/default.png", "Natasha")))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public Message saveMessage(String sid, MessageData messageData) {
        String login = authSessionService.findLoginBySessionId(sid);

        UserEntity user = userRepository.findByLogin(login);
        RoomEntity room = roomRepository.findById(messageData.getRoomId()).get();

        MessageEntity result = messageRepository.save(new MessageEntity(messageData.getText(), LocalDateTime.now(), user, room));

        return new Message(result.getId(), result.getText(), new UserAvatar("default.png", "Natasha"));
    }

    @Override
    public Collection<Message> findLast10PersonalMessages(Integer currentUserId, Integer receiverId) {
        Iterable<PersonalMessageEntity> entities = personalMessageRepository
                .findLast10Message(currentUserId, receiverId);

        // puzzlers
        return StreamSupport.stream(entities.spliterator(), false)
                .map(entity -> modelMapper.map(entity, Message.class))
                .peek(message -> System.out.println(message.getUser()))
                .collect(Collectors.toList());
    }

    @Override
    public Message savePersonalMessage(String sid, Integer receiverId, String text) {
        String login = authSessionService.findLoginBySessionId(sid);

        UserEntity sender = userRepository.findByLogin(login);
        UserEntity receiver = userRepository.findById(receiverId).get();

        PersonalMessageEntity entity = new PersonalMessageEntity();
        entity.setText(text);
        entity.setSendDate(LocalDateTime.now());
        entity.setSender(sender);
        entity.setReceiver(receiver);

        PersonalMessageEntity result = personalMessageRepository.save(entity);
        return modelMapper.map(result, Message.class);
    }
}
