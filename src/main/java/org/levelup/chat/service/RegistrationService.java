package org.levelup.chat.service;

import org.levelup.chat.domain.dto.RegistrationRequest;

public interface RegistrationService {

    void register(RegistrationRequest request);

}
