package org.levelup.chat.service;

import org.levelup.chat.domain.dto.Room;

import java.util.Collection;

public interface RoomService {

    Collection<Room> findAll();

    Room findById(Integer roomId);

}
