package org.levelup.chat.service;

public interface AuthenticationService {

    boolean authenticate(String login, String password);



}
