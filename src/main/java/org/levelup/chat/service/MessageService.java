package org.levelup.chat.service;

import org.levelup.chat.domain.dto.Message;
import org.levelup.chat.domain.dto.MessageData;

import java.util.Collection;

public interface MessageService {

    Collection<Message> findAllMessagesInRoom(Integer roomId);

    Message saveMessage(String sid, MessageData messageData);

    Collection<Message> findLast10PersonalMessages(Integer currentUserId, Integer receiverId);

    Message savePersonalMessage(String sid, Integer receiverId, String text);

}
