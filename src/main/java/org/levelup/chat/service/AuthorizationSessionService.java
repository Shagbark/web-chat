package org.levelup.chat.service;

import org.levelup.chat.domain.dto.UserSession;
import org.levelup.chat.domain.entity.UserEntity;

/**
 * @author protsko on 27/06/2019
 */
public interface AuthorizationSessionService {

    UserSession createOrUpdateSession(String login);

    boolean isExpired(String sid);

    String findLoginBySessionId(String sid);

    Integer findUserIdBySessionId(String sid);

    void removeSession(String sid);

    String getAuthenticatedUser();

    void setAuthenticatedUser(String login);

}
