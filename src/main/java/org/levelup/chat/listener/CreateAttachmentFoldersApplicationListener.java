package org.levelup.chat.listener;

import lombok.SneakyThrows;
import org.levelup.chat.domain.dto.User;
import org.levelup.chat.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.nio.file.Files;
import java.nio.file.Paths;

@Component
public class CreateAttachmentFoldersApplicationListener implements ApplicationListener<ContextRefreshedEvent> {

    @Value("${web.chat.attachment}")
    private String attachmentsPath;
    @Value("${web.chat.attachment.avatars}")
    private String avatarsPath;

    private final UserService userService;

    @Autowired
    public CreateAttachmentFoldersApplicationListener(UserService userService) {
        this.userService = userService;
    }

    @SneakyThrows
    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        boolean attachmentFolderExist = Files.exists(Paths.get(attachmentsPath));
        boolean avatarFolderExist = Files.exists(Paths.get(avatarsPath));

//        if (attachmentFolderExist && avatarFolderExist) {
//            return;
//        }

        if (!attachmentFolderExist) {
            createFolder(attachmentsPath);
        }
        if (!avatarFolderExist) {
            createFolder(avatarsPath);
        }
        for (User user : userService.findAll()) {
            if (!Files.exists(Paths.get(attachmentsPath + user.getLogin()))) {
                Files.createDirectory(Paths.get(attachmentsPath + user.getLogin()));
            }
        }
    }

    @SneakyThrows
    private void createFolder(String path) {
        Files.createDirectories(Paths.get(path));
    }

}
