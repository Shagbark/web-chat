package org.levelup.chat.domain.dto;

import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Message {

    private Integer id;
    private String text;
    private UserAvatar user;

}
