package org.levelup.chat.domain.dto;

import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class UserAvatar {

    private String avatar;
    private String name;

}
